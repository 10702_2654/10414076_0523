package sample;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.util.Duration;

import java.net.URL;
import java.sql.Time;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

public class Controller implements Initializable {
    public Button btnWorking;
    public ProgressBar pbWorking;
    public ImageView imageView1;
    public ProgressIndicator piWorking;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        //pbWorking.setProgress(ProgressBar.INDETERMINATE_PROGRESS);
        //pbWorking.setVisible(false);
        //imageView1.setImage(new Image(getClass().getResource("/resources/0.png").toString()));
    }

    public void onWorking(ActionEvent actionEvent) {

        Timeline timeline = new Timeline();
        timeline.setCycleCount(Timeline.INDEFINITE);
        timeline.setAutoReverse(false);

        for (int j = 0;j<4;j++){
            int k = j;
            timeline.getKeyFrames().add(
                    new KeyFrame(Duration.seconds(j + 1), new EventHandler<ActionEvent>() {
                        @Override
                        public void handle(ActionEvent event) {
                            imageView1.setImage(new Image(getClass().getResource("/resources/".concat(String.valueOf(k).concat(".png"))).toString()));
                        }
                    })
            );
        }

        Task task = new Task<Void>(){

            @Override
            protected Void call() throws Exception {
                final int max = 100;
                for (int i = 1;i<=max;i++){
                    Thread.sleep(100);
                    updateProgress(i,max);
                }
                return null;
            }

            @Override
            protected void scheduled() {
                super.scheduled();
                pbWorking.setVisible(true);

                timeline.play();
            }

            @Override
            protected void succeeded() {
                super.succeeded();
                pbWorking.setVisible(false);
                piWorking.setVisible(false);
                imageView1.setVisible(false);

                timeline.stop();
            }

            @Override
            protected void failed() {
                super.failed();
            }
        };

        pbWorking.progressProperty().bind(task.progressProperty());
        piWorking.progressProperty().bind(task.progressProperty());
        new Thread(task).start();

        /*Timeline timeline = new Timeline();
        timeline.setCycleCount(Timeline.INDEFINITE);
        timeline.setAutoReverse(false);

        for (int i = 0;i<4;i++){
            int j = i;
            timeline.getKeyFrames().add(
                    new KeyFrame(Duration.seconds(i + 1), new EventHandler<ActionEvent>() {
                        @Override
                        public void handle(ActionEvent event) {
                            imageView1.setImage(new Image(getClass().getResource("/resources/".concat(String.valueOf(j).concat(".png"))).toString()));
                        }
                    })
            );
        }

        timeline.play();*/

    }
}
